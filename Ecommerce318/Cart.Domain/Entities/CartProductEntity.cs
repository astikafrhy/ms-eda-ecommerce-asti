﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Entities
{
    public class CartProductEntity
    {
        public Guid Id { get; set; }    
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        //public string Sku { get; set; }
        //public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CartId")]
        public virtual CartEntity Cart { get; set; }
        [ForeignKey("ProductId")]
        public virtual ProductEntity Product { get; set; }
    }
}
