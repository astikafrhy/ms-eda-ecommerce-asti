﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class CartQuery
    {
        private readonly ICartService _service;
        public CartQuery(ICartService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CartDto>> GetAllCartAsync()
        {
            IEnumerable<CartDto> result = await _service.All();
            return result;
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            return await _service.GetCartById(id);
        }
    }
}
