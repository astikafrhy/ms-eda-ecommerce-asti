﻿using AutoMapper;
using Framewok.Core.Events;
using Framewok.Core.Events.Externals;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Entities.Configurations;
using Store.Domain.EventEnvelopes;
using Store.Domain.Repository;

namespace Store.Domain.Services
{
    public interface IProductServices
    {
        Task<IEnumerable<ProductDtos>> All();
        Task<ProductDtos> GetProductById(Guid id);
        Task<ProductDtos> AddProduct(ProductDtos dto);
        Task<bool> UpdateProduct(ProductDtos dto);
        Task<bool> UpdateProductStatus(Guid id, StoreStatusEnum status);
        Task<bool> DeleteProduct(Guid id);
    }
    public class ProductService : IProductServices
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public ProductService(IProductRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<ProductDtos> AddProduct(ProductDtos dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<ProductEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<ProductCreated>(
                    ProductCreated.Create(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.Sku,
                        entity.Name,
                        entity.Description,
                        entity.Price,
                        entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Status,
                        entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<ProductDtos>(entity);
                }
            }
            return new ProductDtos();
        }
        public async Task<IEnumerable<ProductDtos>> All()
        {
        return _mapper.Map<IEnumerable<ProductDtos>>(await _repository.GetAll());
        }

        public async Task<ProductDtos> GetProductById(Guid id)
        {
            return _mapper.Map<ProductDtos>(await _repository.GetById(id));

        }

        public async Task<bool> UpdateProduct(ProductDtos dto)
        {
             if (dto != null)
             {
                var products = await _repository.GetById(dto.Id);
                if (products != null)
                {
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(dto));
                    entity.Status = products.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<ProductUpdated>(
                        ProductUpdated.Update(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.Sku,
                        entity.Name,
                        entity.Description,
                        entity.Price,
                        entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Modified
                        )
                    );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateProductStatus(Guid id, StoreStatusEnum status)
        {
            var product = await _repository.GetById(id);
            if (product != null)
            {
                product.Status = status;
                var entity = await _repository.Update(product);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }
            return false;
        }

        public async Task<bool> DeleteProduct(Guid id)
        {
            if (id != null)
            {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = StoreStatusEnum.Remove;
                    _repository.delete(category);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;

        }


    }
}
