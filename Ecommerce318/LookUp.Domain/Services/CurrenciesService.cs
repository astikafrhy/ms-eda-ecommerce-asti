﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Services
{
    public interface ICurrenciesService
    {
        Task<IEnumerable<CurrenciesDto>> All();
        Task<CurrenciesDto> GetCurrenciesById(Guid id);
        Task<CurrenciesDto> AddCurrencies(CurrenciesDto dto);
        Task<bool> UpdateCurrencies(CurrenciesDto dto);
        Task<bool> UpdateCurrenciesStatus(Guid id, LookUpStatusEnum status);
    }
    public class CurrenciesService : ICurrenciesService
    {
        private ICurrenciesRepository _repository;
        private readonly IMapper _mapper;
        public CurrenciesService(ICurrenciesRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CurrenciesDto>> All()
        {
            return _mapper.Map<IEnumerable<CurrenciesDto>>(await _repository.GetAll());
        }

        public async Task<CurrenciesDto> GetCurrenciesById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CurrenciesDto>(result);
                }
            }
            return null;
        }

        public async Task<CurrenciesDto> AddCurrencies(CurrenciesDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoEntity = _mapper.Map<CurrenciesEntity>(dto);
                var entity = await _repository.Add(dtoEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<CurrenciesDto>(entity);
                }

            }
            return new CurrenciesDto();
        }

        public async Task<bool> UpdateCurrencies(CurrenciesDto dto)
        {
            if (dto != null)
            {
                var currencies = await _repository.GetById(dto.Id);
                dto.Status = currencies.Status;
                if (currencies != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CurrenciesEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                        return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateCurrenciesStatus(Guid id, LookUpStatusEnum status)
        {
            var currencies = await _repository.GetById(id);
            
            if (currencies != null)
            {
                currencies.Status = status;
                var entity = await _repository.Update(currencies);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }
            return false;
        }
    }
}
