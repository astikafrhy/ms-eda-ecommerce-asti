﻿using User.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Framework.Auth;

namespace User.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<UserEntity>> GetAll();
        Task<IEnumerable<UserEntity>> GetPaged(int page, int size);
        Task<UserEntity> GetById(Guid id);
        Task<UserEntity> Add(UserEntity entity);
        Task<UserEntity> Update(UserEntity entity);
        void Delete(UserEntity entity);
        Task<UserEntity> Login(string userName, string password);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class UserRepository : IUserRepository
    {
        protected readonly UserDBContext _context;
        public UserRepository(UserDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<UserEntity> Add(UserEntity entity)
        {
            _context.Set<UserEntity>().Add(entity);
            return entity;
        }

        public void Delete(UserEntity entity)
        {
            _context.Set<UserEntity>().Update(entity);
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            return await _context.Set<UserEntity>().Where(u => u.Status != UserStatusEnum.Removed).ToListAsync();
        }

        public async Task<UserEntity> GetById(Guid id)
        {
            return await _context.Set<UserEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UserEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<UserEntity> Update(UserEntity entity)
        {
            _context.Set<UserEntity>().Update(entity);
            return entity;
        }
        public async Task<UserEntity> Login(string userName, string password)
        {
            return await _context.Set<UserEntity>()
               .FirstOrDefaultAsync(o => o.UserName == userName && o.Password == Encryption.HashSha256(password));
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
