﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Atribute
{
    public record AtributeCreated(
        Guid Id,
        AtributeTypeEnum Type,
        string Unit, 
        LookUpStatusEnum Status,
        DateTime Modified
    )
    {
            public static AtributeCreated Create(
                Guid id,
                AtributeTypeEnum type,
                string unit,
                LookUpStatusEnum status,
                DateTime modified
            ) => new(id,type,unit,status, modified);
    }

}
