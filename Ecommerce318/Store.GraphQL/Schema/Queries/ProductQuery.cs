﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.GraphQL.Schema.Queries;

namespace Store.GraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class ProductQuery
    {
        private readonly IProductServices _services;

        public ProductQuery(IProductServices services)
        {
            _services = services;
        }

        [Authorize]
        public async Task<IEnumerable<ProductDtos>> GetAllProductAsync()
        {
            IEnumerable<ProductDtos> result = await _services.All();
            return result;
        }

        [Authorize]
        public async Task<ProductDtos> GetProductById(Guid id)
        {
            return await _services.GetProductById(id);
        }
    }
}
