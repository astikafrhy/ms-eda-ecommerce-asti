using FluentValidation;
using Framewok.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repository;
using Store.Domain.Services;
using Store.Domain.Validations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build()
        .GetSection("ConnectionStrings")
        .GetSection("Store_Db_Conn").Value);

    options
    .EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

});
// Add services to the container.
builder.Services
    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryServices, CategoryService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductServices, ProductService>();

builder.Services.AddValidatorsFromAssemblyContaining<CategoryValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<ProductValidator>();

builder.Services.AddControllers();

builder.Services.AddStore();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
