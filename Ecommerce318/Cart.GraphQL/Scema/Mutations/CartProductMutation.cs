﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class CartProductMutation
    {
        private readonly ICartProductService _service;
        public CartProductMutation(ICartProductService service)
        {
            _service = service;
        }

        public async Task<CartProductDto> AddCartProductAsync(AddCartProductDataType cartProduct)
        {
            CartProductDto dto = new CartProductDto()
            {
                CartId = cartProduct.CartId,
                ProductId = cartProduct.ProductId,
                Quantity = cartProduct.Quantity
            };
            return await _service.AddCartProduct(dto);
        }
    }
}
