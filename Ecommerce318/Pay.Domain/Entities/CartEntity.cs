﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Entities
{
    public class CartEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public decimal Total { get; set; }
        public decimal Pay { get; set; }
        public CartStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; set; } = DateTime.Now;

        [ForeignKey("CustomerId")]
        public virtual UserEntity Customer { get; set; }
    }
}
