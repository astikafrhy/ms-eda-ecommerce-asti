﻿
namespace User.Domain.EventEnvelopes.User
{
    public record UserCreated
        (
            Guid Id,
            string UserName,
            string Password,
            string FirstName,
            string LastName,
            string Email,
            UserTypeEnum type,
            UserStatusEnum Status,
            DateTime Modified
        )
    {
        public static UserCreated Create
            (
                Guid id,
                string userName,
                string password,
                string firstName,
                string lastName,
                string email,
                UserTypeEnum type,
                UserStatusEnum status,
                DateTime modified
            ) => new(id, userName, password, firstName, lastName, email, type, status, modified);
    }
}

