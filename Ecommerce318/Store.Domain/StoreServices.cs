﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Store.Domain.Projections;

namespace Store.Domain
{
    public static class StoreServices
    {
        public static IServiceCollection AddStore(this IServiceCollection services) =>
            services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) =>
            services //.AddScoped<AttributeProjection>();
            .Projection(builder => builder
                .AddOn<AtributeCreated>(AtributeProjection.Handle)
                .AddOn<AtributeUpdated>(AtributeProjection.Handle)
                .AddOn<AtributeStatusChanged>(AtributeProjection.Handle)
                //.AddOn<CurrencyCreated>(CurrencyProjection.Handle)
            );
    }
}
