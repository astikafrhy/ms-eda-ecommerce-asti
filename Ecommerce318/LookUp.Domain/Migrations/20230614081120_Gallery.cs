﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LookUp.Domain.Migrations
{
    /// <inheritdoc />
    public partial class Gallery : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "GalleryId",
                table: "Atributes",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Galeries",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    FileLink = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Modified = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Galeries", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Atributes_GalleryId",
                table: "Atributes",
                column: "GalleryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Atributes_Galeries_GalleryId",
                table: "Atributes",
                column: "GalleryId",
                principalTable: "Galeries",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Atributes_Galeries_GalleryId",
                table: "Atributes");

            migrationBuilder.DropTable(
                name: "Galeries");

            migrationBuilder.DropIndex(
                name: "IX_Atributes_GalleryId",
                table: "Atributes");

            migrationBuilder.DropColumn(
                name: "GalleryId",
                table: "Atributes");
        }
    }
}
