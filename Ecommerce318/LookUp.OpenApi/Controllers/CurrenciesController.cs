﻿using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LookUp.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        private readonly ICurrenciesService _service;
        private readonly ILogger<CurrenciesController> _logger;

        public CurrenciesController(ICurrenciesService service, ILogger<CurrenciesController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CurrenciesDto payload, CancellationToken cancellationToken)
        {
            try
            {
                var dto = await _service.AddCurrencies(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CurrenciesDto payload, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateCurrencies(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                var isUpdated = await _service.UpdateCurrenciesStatus(id, status);
                if (isUpdated)
                    return Ok(isUpdated);

            }
            catch (OperationCanceledException ex) when
            (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }
    }
}

