﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelopes
{
    public record ProductCreated(
       Guid Id,
       Guid CategoryId,
       Guid AttributeId,
       string SKU,
       string Name,
       string Description,
       decimal Price,
       decimal Volume,
       int Sold,
       int Stock,
       StoreStatusEnum Status,
       DateTime Modified
       )
    {
        public static ProductCreated Create(
             Guid id,
        Guid categoryId,
        Guid attributeId,
        string sku,
        string name,
        string description,
        decimal price,
        decimal volume,
        int sold,
        int stock,
        StoreStatusEnum status,
        DateTime modified
            ) => new(id, categoryId, attributeId, sku, name,
                description, price, volume, sold, stock, status, modified);
    }

}
