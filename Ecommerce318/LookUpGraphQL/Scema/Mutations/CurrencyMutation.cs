﻿using HotChocolate.Authorization;
using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUpGraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class CurrencyMutation
    {
        private readonly ICurrenciesService _service;

        public CurrencyMutation(ICurrenciesService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> AddCurrencyAsync(CurrencyTypeInput currency)
        {
            CurrenciesDto dto = new CurrenciesDto();
            dto.Name = currency.Name;
            dto.Code = currency.Code;
            dto.Symbol = currency.Symbol;
            var result = await _service.AddCurrencies(dto);
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> EditCurrencyAsync(Guid id, CurrencyTypeInput currency)
        {
            CurrenciesDto dto = new CurrenciesDto();
            dto.Id = id;
            dto.Name = currency.Name;
            dto.Code = currency.Code;
            dto.Symbol = currency.Symbol;
            var result = await _service.UpdateCurrencies(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> UpdateCurrencyStatusAsync(Guid id, LookUpStatusEnum status)
        {
            var result = await _service.UpdateCurrenciesStatus(id, status);
            if (result)
            {
                return await _service.GetCurrenciesById(id);
            }
            else
            {
                throw new GraphQLException(new Error("Currency not found", "404"));

            }

        }

    }
}
