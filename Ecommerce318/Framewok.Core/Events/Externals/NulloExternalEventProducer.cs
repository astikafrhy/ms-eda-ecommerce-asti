﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framewok.Core.Events.Externals
{
    public class NulloExternalEventProducer : IExternalEventProducer
    {
        Task IExternalEventProducer.Publish(IEventEnvelope @event, CancellationToken ct)
        {
            return Task.CompletedTask;
        }
    }
}
