﻿namespace LookUpGraphQL.Scema.Mutations
{
    public class CurrencyTypeInput
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }    
    }
}
