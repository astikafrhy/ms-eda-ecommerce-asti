﻿using System.Text.Json.Serialization;

namespace Cart.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum CartStatusEnum
    {
        Pending,
        Confirmed,
        Paid,
        Canceled
    }
}
