﻿namespace Payment.Domain.EventEnvelopes
{
    public record PaymentCreated(
        Guid CartId,
        List<CartProductItem> CartProducts,
        CartStatusEnum Status
    )
    {
        public static PaymentCreated Create(
            Guid cartId,
            List<CartProductItem> cartProducts,
            CartStatusEnum status
        ) => new(cartId, cartProducts, status);
    }

    public class CartProductItem
    {
        public Guid ProductId { get; set; }
        public int Stock { get; set; }
        public int Quantity { get; set; }
    }
}
