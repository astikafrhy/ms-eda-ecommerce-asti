﻿using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class CartMutation
    {
        private readonly ICartService _service;
        public CartMutation(ICartService service)
        {
            _service = service;
        }

        public async Task<CartDto> AddCartAsync(Guid customerId)
        {
            CartDto dto = new CartDto();
            dto.CustomerId = customerId;
            return await _service.AddCart(dto);
        }

        public async Task<CartDto> ConfirmCartAsync(Guid id)
        {
            CartDto dto = new CartDto();
            dto.Id = id;
            var result = await _service.UpdatCartStatus(id, CartStatusEnum.Confirmed);
            if (result)
                dto = await _service.GetCartById(id);

            return dto;
        }
    }
}
