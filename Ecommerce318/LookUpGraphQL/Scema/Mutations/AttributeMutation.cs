﻿using HotChocolate.Authorization;
using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUpGraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class AttributeMutation
    {
        private readonly IAtributeService _service;   

        public AttributeMutation(IAtributeService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] {"admin"})]
        public async Task<AtributeDto> AddAttributeAsync(AttributeTypeInput attribute)
        {
            AtributeDto dto = new AtributeDto();
            dto.Unit = attribute.Unit;  
            dto.Type = attribute.Type;
            var result = await _service.AddAtribute(dto);
            return result;
        }
        [Authorize(Roles = new[] {"admin"})]
        public async Task<AtributeDto> EditAttributeAsync(Guid id, AttributeTypeInput attribute)
        {
            AtributeDto dto = new AtributeDto();
            dto.Id = id;
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.UpdateAtribute(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AtributeDto> UpdateAttributeStatusAsync(Guid id, LookUpStatusEnum status )
        {
            var result = await _service.UpdateAtributeStatus(id, status);
            if (result)
            {
                return await _service.GetAtributeById(id);
            }
            else
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));  
            }
            
        }

    }
}
