using Framework.Kafka;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using User.Domain;
using User.Domain.MapProfile;
using User.Domain.Repositories;
using User.Domain.Services;
using User.GraphQL.Scema.Mutations;
using User.GraphQL.Scema.Queries;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("User_Db_Conn").Value);

    options
    .EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

});

builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer("Bearer", opt => {
    var Configuration = builder.Configuration;

    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = Configuration["JWT:ValidIssuer"],
        ValidAudience = Configuration["JWT:ValidAudience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
    };

    opt.Events = new JwtBearerEvents
    {
        OnChallenge = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account not autherized");
            });
            return Task.CompletedTask;
        },
        OnForbidden = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account forbidden");
            });
            return Task.CompletedTask;
        }
    };
});

builder.Services
    .AddScoped<Query>()
    .AddScoped<UserQuery>()
    .AddScoped<AuthQuery>()

    .AddScoped<Mutation>()
    .AddScoped<UserMutation>()
    
    .AddScoped<IUserRepository, UserRepository>()
    .AddScoped<IUserService, UserService>()
    .AddGraphQLServer()

    .AddQueryType<Query>()
    .AddTypeExtension<UserQuery>()
    .AddTypeExtension<AuthQuery>()


    .AddMutationType<Mutation>()
    .AddTypeExtension<UserMutation>()
    .AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapGraphQL();

app.MapControllers();

app.Run();
