﻿using Cart.Domain.Entities;

namespace Cart.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<ProductEntity> GetById(Guid id);
    }
    public class ProductRepository : IProductRepository
    {
        protected readonly CartDBContext _context;

        public ProductRepository(CartDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<ProductEntity> GetById(Guid id)
        {
            return await _context.Set<ProductEntity>().FindAsync(id);
        }
    }
}
