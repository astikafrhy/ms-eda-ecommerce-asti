﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EvenEnvelopes
{
    public record CartStatusChanged(
       Guid Id,
       Guid CustomerId,
       List<CartProductItem> CartProducts,
        CartStatusEnum Status)
    {
        public static CartStatusChanged UpdateStatus(
            Guid id,
            Guid customerId,
            List<CartProductItem> cartProducts,
            CartStatusEnum status
            ) => new(id, customerId, cartProducts,status);
    }

    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }

}
