﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Entities
{
    public class AtributeEntity
    {
        public Guid Id { get; set; }
        public Guid? GalleryId { get; set; }
        public AtributeTypeEnum Type { get; set; } = default;
        public string Unit { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default;
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("GalleryId")]
        public virtual GalleryEntity Gallery { get; set; }
    }
}
