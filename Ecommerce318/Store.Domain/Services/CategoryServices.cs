﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repository;

namespace Store.Domain.Services
{
    public interface ICategoryServices
    {
        Task<IEnumerable<CategoryDtos>> All();
        Task<CategoryDtos> GetCategoryById(Guid id);
        Task<CategoryDtos> AddCategory(CategoryDtos dto);
        Task<bool> UpdateCategory(CategoryDtos dto);
        Task<bool> UpdateCategoryStatus(Guid id, StoreStatusEnum status);
    }
    public class CategoryService : ICategoryServices
    {
        private ICategoryRepository _repository;
        private readonly IMapper _mapper;
        public CategoryService(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
         
        public async Task<CategoryDtos> AddCategory(CategoryDtos dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoEntity = _mapper.Map<CategoryEntity>(dto);
                var entity = await _repository.Add(dtoEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CategoryDtos>(entity);

            }
            return new CategoryDtos();
        }

        public async Task<IEnumerable<CategoryDtos>> All()
        {
            return _mapper.Map<IEnumerable<CategoryDtos>>(await _repository.GetAll());
        }

        public async Task<CategoryDtos> GetCategoryById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CategoryDtos>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCategory(CategoryDtos dto)
        {
            if (dto != null)
            {
                var category = await _repository.GetById(dto.Id);
                dto.Status = category.Status;
                if (category != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                        return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateCategoryStatus(Guid id, StoreStatusEnum status)
        {
            var category = await _repository.GetById(id);
            //dto.Status = atribute.Status;
            if (category != null)
            {
                category.Status = status;
                var entity = await _repository.Update(category);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }
            return false;
        }
    }
}
