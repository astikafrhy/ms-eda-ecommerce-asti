﻿namespace Store.GraphQL.Scema.Mutations
{
    public class ProductTypeInput
    {
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
    }
}
