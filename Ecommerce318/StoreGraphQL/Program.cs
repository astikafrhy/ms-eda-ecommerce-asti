using Framewok.Core.Events;
using Framework.Kafka; 
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repository;
using Store.Domain.Services;
using StoreGraphQL.Scema.Mutations;
using StoreGraphQL.Scema.Queries;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

    options
    .EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

});

builder.Services.AddStore();
builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer("Bearer", opt => {
    var Configuration = builder.Configuration;

    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = Configuration["JWT:ValidIssuer"],
        ValidAudience = Configuration["JWT:ValidAudience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
    };

    opt.Events = new JwtBearerEvents
    {
        OnChallenge = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account not autherized");
            });
            return Task.CompletedTask;
        },
        OnForbidden = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account forbidden");
            });
            return Task.CompletedTask;
        }
    };
});


builder.Services
    .AddScoped<Query>()
    .AddScoped<CategoryQuery>()
    .AddScoped<ProductQuery>()

    .AddScoped<Mutation>()
    .AddScoped<CategoryMutation>()
    .AddScoped<ProductMutation>()

    .AddScoped<ICategoryRepository, CategoryRepository>()
    .AddScoped<ICategoryServices, CategoryService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductServices, ProductService>()
    .AddGraphQLServer()

    .AddQueryType<Query>()
    .AddTypeExtension<CategoryQuery>()
    .AddTypeExtension<ProductQuery>()


    .AddMutationType<Mutation>()
    .AddTypeExtension<CategoryMutation>()
    .AddTypeExtension<ProductMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
