﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Services
{

    public interface IGalleryService
    {
        Task<IEnumerable<GalleryDto>> All();
        Task<GalleryDto> GetGalleryById(Guid id);
        Task<GalleryDto> AddGallery(GalleryDto dto);
    }
    public class GalleryService : IGalleryService
    {
        private IGalleryRepository _repository;
        private readonly IMapper _mapper;
        public GalleryService(IGalleryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<GalleryDto>> All()
        {
            return _mapper.Map<IEnumerable<GalleryDto>>(await _repository.GetAll());
        }

        public Task<GalleryDto> GetGalleryById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<GalleryDto> AddGallery(GalleryDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Active;
                var dtoEntity = _mapper.Map<GalleryEntity>(dto);
                var entity = await _repository.Add(dtoEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<GalleryDto>(entity);
                }

            }
            return new GalleryDto();
        }

    }
}
