﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services, 
                           Action<DbContextOptionsBuilder> optionAction,
                           ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
                           ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<LookUpDBContext>(optionAction, contextLifetime, optionLifetime);
        }
    }
}
