﻿using Cart.Domain.Projections;
using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;

namespace Store.Domain
{
    public static class CartServices
    {
        public static IServiceCollection AddCart(this IServiceCollection services) =>
            services
                .Projection(builder => builder
                    .AddOn<ProductCreated>(ProductProjection.Handle)
                    .AddOn<UserCreated>(UserProjection.Handle)
            );
    }
}
