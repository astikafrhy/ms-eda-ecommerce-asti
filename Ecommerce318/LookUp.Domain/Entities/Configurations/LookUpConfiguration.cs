﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Entities.Configurations
{
    public class AtributeConfiguration : IEntityTypeConfiguration<AtributeEntity>
    {
        public void Configure(EntityTypeBuilder<AtributeEntity> builder)
        {
            //throw new NotImplementedException();

            builder.ToTable("Atributes");
            builder.HasKey(x => x.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.GalleryId).IsRequired(false);
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Unit).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Status).IsRequired();

        }
    }
    public class CurrenciesConfiguration : IEntityTypeConfiguration<CurrenciesEntity>
    {
        public void Configure(EntityTypeBuilder<CurrenciesEntity> builder)
        {
            //throw new NotImplementedException();

            builder.ToTable("Currencies");
            builder.HasKey(x => x.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Code).HasMaxLength(3).IsRequired();
            builder.Property(e => e.Symbol).HasMaxLength(3).IsRequired();
            builder.Property(e => e.Status).IsRequired();

        }
        
    }

    public class GalleryConfiguration : IEntityTypeConfiguration<GalleryEntity>
    {
        public void Configure(EntityTypeBuilder<GalleryEntity> builder)
        {
            //throw new NotImplementedException();

            builder.ToTable("Galeries");
            builder.HasKey(x => x.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.FileLink).HasMaxLength(255).IsRequired();
            builder.Property(e => e.Description).HasMaxLength(250);
            builder.Property(e => e.Status).IsRequired();

        }

    }
}
