﻿using System.Text.Json.Serialization;

namespace LookUp.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AtributeTypeEnum
    {
        Text,
        Number,
        Decimal
    }
}
