﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framewok.Core.Events.Externals
{
    public  interface IExternalEventProducer
    {
        Task Publish(IEventEnvelope @event, CancellationToken ct);
    }
}
