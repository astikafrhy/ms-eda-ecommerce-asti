﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using Framewok.Core.Events.Externals;

namespace Cart.Domain.Services
{
    public interface ICartProductService
    {
        Task<CartProductDto> AddCartProduct(CartProductDto dto);
        Task<IEnumerable<CartProductDto>> All();
        Task<CartProductDto> GetCartProductById(Guid id);
        Task<bool> UpdatCartStatus(Guid id, CartStatusEnum status);
    }

    public class CartProductService : ICartProductService
    {
        private ICartProductRepository _repository;
        private IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartProductService(ICartProductRepository repository, IProductRepository productRepository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _productRepository = productRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<CartProductDto> AddCartProduct(CartProductDto dto)
        {
            var product = await _productRepository.GetById(dto.ProductId);

            if (product == null)
                return null;

            dto.Price = product.Price;

            var existCp = await _repository.GetByCartId(dto.CartId, dto.ProductId);
            if (existCp != null)
            {
                if (product.Stock >= dto.Quantity + existCp.Quantity)
                    existCp.Quantity = dto.Quantity + existCp.Quantity;
                else
                    existCp.Quantity = product.Stock;

                var entity = await _repository.Update(existCp);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CartProductDto>(entity);
            }
            else
            {
                var entity = await _repository.Add(_mapper.Map<CartProductEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                    return _mapper.Map<CartProductDto>(entity);
            }

            return new CartProductDto();
        }

        public async Task<IEnumerable<CartProductDto>> All()
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _repository.GetAll());
        }

        public async Task<CartProductDto> GetCartProductById(Guid id)
        {
            return _mapper.Map<CartProductDto>(await _repository.GetById(id));
        }

        public Task<bool> UpdatCartStatus(Guid id, CartStatusEnum status)
        {
            throw new NotImplementedException();
        }
    }
}
