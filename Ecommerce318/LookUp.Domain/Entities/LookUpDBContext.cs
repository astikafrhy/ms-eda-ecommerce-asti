﻿using LookUp.Domain.Entities.Configurations;
using Microsoft.EntityFrameworkCore;

namespace LookUp.Domain.Entities
{
    public class LookUpDBContext : DbContext
    {
        public LookUpDBContext(DbContextOptions<LookUpDBContext> options) : base(options)
        {

        }

        public DbSet<AtributeEntity> Atributes { get; set; }
        public DbSet<CurrenciesEntity> Currencies { get; set; }
        public DbSet<CurrenciesEntity> Galleries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AtributeConfiguration());
            modelBuilder.ApplyConfiguration(new CurrenciesConfiguration());
            modelBuilder.ApplyConfiguration(new GalleryConfiguration());

        }


    }
}
