﻿using Framewok.Core.Events;
using Store.Domain.Entities;

namespace Store.Domain.Projections
{
    public record AtributeCreated(
        Guid? Id,
        AtributeTypeEnum Type,
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified
    );

    public record AtributeUpdated(
        Guid? Id,
        AtributeTypeEnum Type,
        string Unit,
        DateTime Modified
    );

    public record AtributeStatusChanged(
        Guid? Id,
        LookUpStatusEnum Status,
        DateTime Modified
    );

    public class AtributeProjection
    {
        private readonly StoreDBContext _context;
        public AtributeProjection()
        {

        }

        public static bool Handle(EventEnvelope<AtributeCreated> eventEnvelope)
        {
            var (id, type, unit, status, modified) = eventEnvelope.Data;
            using (var context = new StoreDBContext(StoreDBContext.OnConfigure()))
            {
                AtributeEntity entity = new AtributeEntity()
                {
                    Id = (Guid)id,
                    Type = type,
                    Unit = unit,
                    Status = status,
                    Modified = modified
                };

                context.Attributes.Add(entity);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<AtributeUpdated> eventEnvelope)
        {
            var (id, type, unit, modified) = eventEnvelope.Data;
            using (var context = new StoreDBContext(StoreDBContext.OnConfigure()))
            {
                AtributeEntity entity = context.Set<AtributeEntity>().Find(id);
                entity.Type = type;
                entity.Unit = unit;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<AtributeStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new StoreDBContext(StoreDBContext.OnConfigure()))
            {
                AtributeEntity entity = context.Set<AtributeEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}
