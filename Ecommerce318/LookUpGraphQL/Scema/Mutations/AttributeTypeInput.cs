﻿using LookUp.Domain;

namespace LookUpGraphQL.Scema.Mutations
{
    public class AttributeTypeInput
    {
        public AtributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
    }
}
