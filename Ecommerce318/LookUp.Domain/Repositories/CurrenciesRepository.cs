﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Repositories
{
    public interface ICurrenciesRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<CurrenciesEntity>> GetAll();
        Task<IEnumerable<CurrenciesEntity>> GetPaged(int page, int size);
        Task<CurrenciesEntity> GetById(Guid id);
        Task<CurrenciesEntity> Add(CurrenciesEntity entity);
        Task<CurrenciesEntity> Update(CurrenciesEntity entity);
        void delete(CurrenciesEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class CurrenciesRepository : ICurrenciesRepository
    {
        protected readonly LookUpDBContext _context;
        public CurrenciesRepository(LookUpDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<CurrenciesEntity> Add(CurrenciesEntity entity)
        {
            _context.Set<CurrenciesEntity>().Add(entity);
            return entity;
        }

        public void delete(CurrenciesEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CurrenciesEntity>> GetAll()
        {
            return await _context.Set<CurrenciesEntity>().ToListAsync();
        }

        public async Task<CurrenciesEntity> GetById(Guid id)
        {
            return await _context.Set<CurrenciesEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CurrenciesEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CurrenciesEntity> Update(CurrenciesEntity entity)
        {
            _context.Set<CurrenciesEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
