﻿namespace Payment.Domain.Entities
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; }
    }
}
