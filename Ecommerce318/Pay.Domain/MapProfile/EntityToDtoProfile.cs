﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity To Dto profile")
        {
            CreateMap<CartEntity, CartDto>();
            CreateMap<UserEntity, UserDto>();

            CreateMap<CartProductEntity, CartProductItem>();
        }
    }
}
