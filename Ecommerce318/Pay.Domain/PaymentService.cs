﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Projections;

namespace Payment.Domain
{
    public static class PaymentService
    {
        public static IServiceCollection AddPayment(this IServiceCollection services) =>
            services
                .Projection(builder => builder
                    .AddOn<UserCreated>(UserProjection.Handle)
                    .AddOn<ProductCreated>(ProductProjection.Handle)
                    .AddOn<CartCreated>(CartProjection.Handle)
                    .AddOn<CartStatusChanged>(CartProjection.Handle)
            );
    }
}
