﻿using AutoMapper;
using Framewok.Core.Events;
using Framewok.Core.Events.Externals;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.EventEnvelopes.Atribute;
using LookUp.Domain.Repositories;

namespace LookUp.Domain.Services
{
    public interface IAtributeService
    {
        Task<IEnumerable<AtributeDto>> All();
        Task<AtributeDto> GetAtributeById(Guid id);
        Task<AtributeDto> AddAtribute(AtributeDto dto);
        Task<bool> UpdateAtribute(AtributeDto dto);
        Task<bool> UpdateAtributeStatus(Guid id, LookUpStatusEnum status);

    }

    public class AtributeService : IAtributeService
    {
        private  IAtributeRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public AtributeService(IAtributeRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;

        }
        public async Task<AtributeDto> AddAtribute(AtributeDto dto)
        {
            if(dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoEntity = _mapper.Map<AtributeEntity>(dto);
                var entity = await _repository.Add(dtoEntity);
                var result = await _repository.SaveChangesAsync();


                if(result > 0)
                {
                    var externalEvent = new EventEnvelope<AtributeCreated>(
                    AtributeCreated.Create(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Status,
                        entity.Modified
                        )
                    );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<AtributeDto>(entity);
                }
                return new AtributeDto();
            }
            return new AtributeDto();
        }

        public async Task<IEnumerable<AtributeDto>> All()
        {
            return _mapper.Map<IEnumerable<AtributeDto>>(await _repository.GetAll());
        }

        public async Task<AtributeDto> GetAtributeById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if(result != null)
                {
                    return _mapper.Map<AtributeDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateAtribute(AtributeDto dto)
        {
            if(dto != null)
            {
                
                var atribute = await _repository.GetById(dto.Id);
                dto.Status = atribute.Status;
                
                if(atribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<AtributeEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {

                        var externalEvent = new EventEnvelope<AtributeUpdated>(
                        AtributeUpdated.Update(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Modified
                       )
                   );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateAtributeStatus(Guid id, LookUpStatusEnum status)
        {
            var atribute = await _repository.GetById(id);
            //dto.Status = atribute.Status;
            if (atribute != null)
            {
                atribute.Status = status;
                var entity = await _repository.Update(atribute);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }
            return false;
        }

    }
}
