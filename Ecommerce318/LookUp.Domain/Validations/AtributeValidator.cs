﻿using FluentValidation;
using LookUp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Validations
{
    public class AtributeValidator : AbstractValidator<AtributeDto>
    {
        public AtributeValidator()
        {
            RuleFor(x => x.Type).NotEmpty();
            RuleFor(x => x.Unit).MaximumLength(30).WithMessage("Unit max 30 chars");
        }
    }
}
