﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Repositories
{
    public interface IAtributeRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<AtributeEntity>> GetAll();
        Task<IEnumerable<AtributeEntity>> GetPaged(int page, int size);
        Task<AtributeEntity> GetById(Guid id);
        Task<AtributeEntity> Add(AtributeEntity entity);
        Task<AtributeEntity> Update(AtributeEntity entity);
        void delete(AtributeEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    }

    public class AtributeRepository: IAtributeRepository
    {
        protected readonly LookUpDBContext _context;
        public AtributeRepository(LookUpDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<AtributeEntity> Add(AtributeEntity entity)
        {
            _context.Set<AtributeEntity>().Add(entity);
            return entity;
        }

        public void delete(AtributeEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AtributeEntity>> GetAll()
        {
            return await _context.Set<AtributeEntity>().ToListAsync();
        }

        public async Task<AtributeEntity> GetById(Guid id)
        {
            return await _context.Set<AtributeEntity>().FindAsync(id);
        } 

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AtributeEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<AtributeEntity> Update(AtributeEntity entity)
        {
            _context.Set<AtributeEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if(disposing)
                _context.Dispose();
        }
        
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
