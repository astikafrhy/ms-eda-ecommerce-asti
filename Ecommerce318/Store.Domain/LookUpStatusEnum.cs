﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Store.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LookUpStatusEnum
    {
        Active,
        Inactive,
        Removed
    }

}
