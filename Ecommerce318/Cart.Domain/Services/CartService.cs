﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.EvenEnvelopes;
using Cart.Domain.Repositories;
using Framewok.Core.Events;
using Framewok.Core.Events.Externals;

namespace Cart.Domain.Services
{
    public interface ICartService
    {
        Task<CartDto> AddCart(CartDto dto);
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> GetCartById(Guid id);
        Task<bool> UpdatCartStatus(Guid id, CartStatusEnum status);
    }

    public class CartService : ICartService
    {
        private ICartRepository _repository;
        private ICartProductRepository _cartProductRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartService(ICartRepository repository, ICartProductRepository cartProductRepository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _cartProductRepository = cartProductRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<CartDto> AddCart(CartDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Pending;
                var entity = await _repository.Add(_mapper.Map<CartEntity>(dto));
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<CartCreated>(
                            CartCreated.Created(
                                entity.Id,
                                entity.CustomerId,
                                entity.Status
                            )
                        );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<CartDto>(entity);
                }
            }
            return new CartDto();
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            return _mapper.Map<CartDto>(await _repository.GetById(id));
        }

        public async Task<bool> UpdatCartStatus(Guid id, CartStatusEnum status)
        {
            bool res = true;
            var entity = await _repository.GetById(id);

            if (entity != null)
            {
                entity.Status = status;
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    List<CartProductItem> list = new List<CartProductItem>();

                    if (status == CartStatusEnum.Confirmed)
                    {
                        var cartProducts = await _cartProductRepository.GetByCartId(entity.Id);

                        foreach (var item in cartProducts)
                        {
                            list.Add(new CartProductItem()
                            {
                                Id = item.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity
                            });
                        }
                    }

                    var externalEvent = new EventEnvelope<CartStatusChanged>(
                        CartStatusChanged.UpdateStatus(
                            entity.Id,
                            entity.CustomerId,
                            list,
                            entity.Status
                        ));

                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                }
            }
            return res;
        }
    }
}
