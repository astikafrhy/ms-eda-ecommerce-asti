﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Scema.Queries
{
    [ExtendObjectType("Query")]
    public class CategoriesQuery
    {

        private readonly ICategoryServices _services;

        public CategoriesQuery(ICategoryServices services)
        {
            _services = services;
        }

        [Authorize]
        public async Task<IEnumerable<CategoryDtos>> GetAllCategoryAsync()
        {
            IEnumerable<CategoryDtos> result = await _services.All();
            return result;
        }

        [Authorize]
        public async Task<CategoryDtos> GetCategoryByIdAsync(Guid id)
        {
            return await _services.GetCategoryById(id);
        }

    }
}

