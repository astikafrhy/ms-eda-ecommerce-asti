﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;


namespace StoreGraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class ProductQuery
    {
        private readonly IProductServices _service;

        public ProductQuery(IProductServices service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<ProductDtos>> GetAllProductAsync()
        {
            IEnumerable<ProductDtos> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDtos> GetProductById(Guid id)
        {
            return await _service.GetProductById(id);

        }
    }
}
