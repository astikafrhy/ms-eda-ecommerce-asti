﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<StoreDBContext>(optionAction, contextLifetime,
                optionLifetime);
        }
    }
}
