﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Entities
{
    public class CartEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum Status { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CustomerId")]
        public virtual UserEntity Customer { get; set; }
        public virtual ICollection<CartProductEntity> CartProducts { get; set; }
    }
}
