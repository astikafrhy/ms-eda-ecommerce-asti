﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Dtos
{
    public class AtributeDto
    {
        public Guid Id { get; set; }
        public AtributeTypeEnum Type { get; set; } 
        public string Unit { get; set; } 
        public LookUpStatusEnum Status { get; internal set; } 
    }
}
