﻿
using User.Domain.Entities.Configuration;
using Microsoft.EntityFrameworkCore;


namespace User.Domain.Entities
{
    public class UserDBContext : DbContext
    {

        public UserDBContext(DbContextOptions<UserDBContext> options) : base(options)
        {

        }
        public DbSet<UserEntity> Users { get; set; }

        protected void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }


}

