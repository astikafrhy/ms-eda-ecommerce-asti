﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Schema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class PaymentQuery
    {
        private readonly ICartService _service;
        public PaymentQuery(ICartService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CartDto>> GetAllAsync()
        {
            return await _service.All();
        }
    }
}
