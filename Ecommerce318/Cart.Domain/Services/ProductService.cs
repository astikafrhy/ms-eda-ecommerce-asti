﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Repositories;

namespace Cart.Domain.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetById(Guid id);
    }

    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ProductDto> GetById(Guid id)
        {
            return _mapper.Map<ProductDto>(await _repository.GetById(id));
        }
    }
}
