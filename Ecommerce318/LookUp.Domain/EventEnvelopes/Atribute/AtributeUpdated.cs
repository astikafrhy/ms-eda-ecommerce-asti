﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelopes.Atribute
{
    public record AtributeUpdated(
        Guid Id,
        AtributeTypeEnum Type,
        string Unit,
        DateTime Modified
    )
    {
        public static AtributeUpdated Update(
            Guid id,
            AtributeTypeEnum type,
            string unit,
            DateTime modified
        ) => new(id, type, unit, modified);
    }
}
