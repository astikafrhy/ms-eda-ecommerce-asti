﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Entities.Configurations;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile(): base("Entity to Dto profile")
        {
            CreateMap<CategoryEntity, CategoryDtos>();
            CreateMap<CategoryDtos, CategoryEntity>();

            CreateMap<ProductEntity, ProductDtos>();
            CreateMap<ProductDtos, ProductEntity>();

        }
    }
}
