﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class UserQuery
    {
        private readonly IUserService _service;

        public UserQuery(IUserService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<UserDto>> GetAllUserAsync()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            return await _service.GetUserById(id);

        }
    }
}
