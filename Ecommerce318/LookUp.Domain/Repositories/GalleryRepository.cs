﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Repositories
{
    public interface IGalleryRepository
    {
        Task<IEnumerable<GalleryEntity>> GetAll();
        Task<GalleryEntity> Add(GalleryEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class GalleryRepository : IGalleryRepository
    {
        protected readonly LookUpDBContext _context;
        public GalleryRepository(LookUpDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<GalleryEntity> Add(GalleryEntity entity)
        {
            await _context.Set<GalleryEntity>().AddAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<GalleryEntity>> GetAll()
        {
            return await _context.Set<GalleryEntity>().ToListAsync();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
