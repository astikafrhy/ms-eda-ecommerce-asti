﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cart.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<CartEntity> Add(CartEntity entity);
        Task<CartEntity> Update(CartEntity entity);
        Task<CartEntity> GetById(Guid id);
        Task<IEnumerable<CartEntity>> GetAll();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CartRepository : ICartRepository
    {
        protected readonly CartDBContext _context;
        public CartRepository(CartDBContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartEntity> Add(CartEntity entity)
        {
            await _context.Set<CartEntity>().AddAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<CartEntity>> GetAll()
        {
            return await _context.Set<CartEntity>().ToListAsync();
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            return await _context.Set<CartEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public Task<CartEntity> Update(CartEntity entity)
        {
            throw new NotImplementedException();
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
