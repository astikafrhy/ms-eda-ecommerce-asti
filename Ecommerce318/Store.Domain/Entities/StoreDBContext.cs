﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Store.Domain.Entities;
using Store.Domain.Entities.Configurations;

namespace Store.Domain
{
    public class StoreDBContext : DbContext
    {
        public StoreDBContext(DbContextOptions<StoreDBContext> options) : base(options)
        {

        }

        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<AtributeEntity> Attributes { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new AtributeConfiguration());
        }

        public static DbContextOptions<StoreDBContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<StoreDBContext>();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

            return optionsBuilder.Options;
        }

    }
}
