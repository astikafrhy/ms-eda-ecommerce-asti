using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.Entities.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUpGraphQL.Scema.Mutations;
using LookUpGraphQL.Scema.Queries;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);

    options
    .EnableSensitiveDataLogging(false)
    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

});

builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer("Bearer", opt => {
    var Configuration = builder.Configuration;

    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = Configuration["JWT:ValidIssuer"],
        ValidAudience = Configuration["JWT:ValidAudience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
    };

    opt.Events = new JwtBearerEvents
    {
        OnChallenge = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account not autherized");
            });
            return Task.CompletedTask;
        },
        OnForbidden = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account forbidden");
            });
            return Task.CompletedTask;
        }
    };
});

builder.Services
    .AddScoped<Query>()
    .AddScoped<AttributeQuery>()
    .AddScoped<CurrencyQuery>()
   

    .AddScoped<Mutation>()
    .AddScoped<AttributeMutation>()
    .AddScoped<CurrencyMutation>()
    .AddScoped<GalleryMutation>()

    .AddScoped<IAtributeRepository, AtributeRepository>()
    .AddScoped<IAtributeService, AtributeService>()
    .AddScoped<ICurrenciesRepository, CurrenciesRepository>()
    .AddScoped<ICurrenciesService, CurrenciesService>()
    .AddScoped<IGalleryRepository, GalleryRepository>()
    .AddScoped<IGalleryService, GalleryService>()
    .AddGraphQLServer()
    .AddType<UploadType>()

    .AddQueryType<Query>()
    .AddTypeExtension<AttributeQuery>()
    .AddTypeExtension<CurrencyQuery>()

    .AddMutationType<Mutation>()
    .AddTypeExtension<AttributeMutation>()
    .AddTypeExtension<CurrencyMutation>()
    .AddTypeExtension<GalleryMutation>()
    .AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.UseFileServer(new FileServerOptions()
{
    FileProvider = new PhysicalFileProvider(
        System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"Resources/Images")),
    RequestPath = new PathString("/img"),
    EnableDirectoryBrowsing = true
});

app.Run();
