﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface ICartProductRepository
    {
        Task<IEnumerable<CartProductEntity>> GetByCartId(Guid id);
    }

    public class CartProductRepository : ICartProductRepository
    {
        protected readonly PaymentDBContext _context;

        public CartProductRepository(PaymentDBContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CartProductEntity>> GetByCartId(Guid id)
        {
            return await _context.Set<CartProductEntity>().Where(x => x.CartId == id).ToListAsync();
        }
    }
}
