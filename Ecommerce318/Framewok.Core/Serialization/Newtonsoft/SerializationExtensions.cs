﻿using Framework.Core.Serialization.Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framewok.Core.Serialization.Newtonsoft
{
    public class NonDefaultContructorContractResolver: DefaultContractResolver
    {
        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            return JsonObjectContractProvider.UsingNonDefaultConstructor(
                base.CreateObjectContract(objectType),
                objectType,
                base.CreateConstructorParameters
                );
        }
    }
    public static class SerializationExtensions
    {
        public static JsonSerializerSettings WithNonDefaultConstructorContractResolver(this JsonSerializerSettings settings)
        {
            settings.ContractResolver = new NonDefaultContructorContractResolver();
            return settings;
        }

        public static object? FromJson(this string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type, 
                new JsonSerializerSettings().WithNonDefaultConstructorContractResolver());
        }

        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj,
                new JsonSerializerSettings().WithNonDefaultConstructorContractResolver());
        }
    }
}
