﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;

namespace Cart.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto profile")
        {
            CreateMap<CartEntity, CartDto>();
            CreateMap<CartDto, CartEntity>();

            CreateMap<CartProductEntity, CartProductDto>();
            CreateMap<CartProductDto, CartProductEntity>();

            CreateMap<ProductEntity, ProductDto>();
            CreateMap<ProductDto, ProductEntity>();
        }
    }
}
