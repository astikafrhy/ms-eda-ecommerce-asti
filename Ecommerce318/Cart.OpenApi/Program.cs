using Cart.Domain;
using Cart.Domain.Repositories;
using Cart.Domain.Services;
using Framewok.Core.Events;
using Framework.Kafka;
using Cart.Domain.MapProfile;
using Store.Domain;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(builder.Configuration);
builder.Services.AddCart();
builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

builder.Services
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<ICartService, CartService>()
    .AddScoped<ICartProductRepository, CartProductRepository>()
    .AddScoped<ICartProductService, CartProductService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
