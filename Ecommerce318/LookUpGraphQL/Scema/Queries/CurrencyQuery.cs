﻿using HotChocolate.Authorization;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUpGraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class CurrencyQuery
    {
        private readonly ICurrenciesService _service;
        public CurrencyQuery(ICurrenciesService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<CurrenciesDto>> GetAllCurrencyAsync()
        {
            IEnumerable<CurrenciesDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> GetCurrenciesById(Guid id)
        {
            return await _service.GetCurrenciesById(id);
        }
    }
}
