﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EvenEnvelopes
{
    public record CartProductUpdated
    (
        Guid Id,
        Guid CardId,
        int Quantity)
    {
        public static CartProductUpdated Create(
            Guid id,
            Guid cartId,
            int quantity
        ) => new(id, cartId, quantity); 

    }
    
}
