﻿using Store.Domain;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Tests.MoqData
{
    public class CategoryMoqData
    {
        public static List<CategoryEntity> GetCategories()
        {
            return new List<CategoryEntity>
            {
                new CategoryEntity
                {
                    Id = new Guid("EB8BE168-E8F7-43DF-AED5-B9F7B742C3B9"),
                    Name = "Food",
                    Description = "Makanan Utama",
                    Status = StoreStatusEnum.Active
                },

                new CategoryEntity
                {
                    Id = new Guid("3785797F-0E15-4584-BD88-41A234E2775F"),
                    Name = "Drink",
                    Description = "Minuman Segar",
                    Status = StoreStatusEnum.Inactive
                }
            };
        }
    }
}
