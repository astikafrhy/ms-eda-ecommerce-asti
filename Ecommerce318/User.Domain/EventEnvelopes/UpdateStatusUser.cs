﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UpdateStatusUser
        (
            Guid Id,
            UserStatusEnum Status
        )
    {
        public static UpdateStatusUser UpdateStatus
            (
                Guid id,
                UserStatusEnum status
            ) => new UpdateStatusUser(id, status);
    }
}

