﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;


namespace StoreGraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class CategoryQuery
    {
        private readonly ICategoryServices _service;

        public CategoryQuery(ICategoryServices service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<CategoryDtos>> GetAllCategoryAsync()
        {
            IEnumerable<CategoryDtos> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDtos> GetCategoryById(Guid id)
        {
            return await _service.GetCategoryById(id);

        }
    }
}
