﻿using Framewok.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record UserCreated(
        Guid Id,
        string UserName,
        string FirstName,
        string LastName,
        string Email,
        UserStatusEnum Status
    );

    public class UserProjection
    {
        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, userName, firstName, lastName, email, status) = eventEnvelope.Data;
            using (var context = new PaymentDBContext(PaymentDBContext.OnConfigure()))
            {
                UserEntity entity = new()
                {
                    Id = (Guid)id,
                    UserName = userName,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }
            return true;

        }
    }
}
