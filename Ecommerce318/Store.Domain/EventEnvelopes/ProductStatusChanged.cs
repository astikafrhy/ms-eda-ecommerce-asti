﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelopes
{
    public record ProductStatusChanged(
        Guid Id,
        StoreStatusEnum Status,
        DateTime Modified
        )
    {
        public static ProductStatusChanged UpdateStatus(
            Guid id,
            StoreStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}
