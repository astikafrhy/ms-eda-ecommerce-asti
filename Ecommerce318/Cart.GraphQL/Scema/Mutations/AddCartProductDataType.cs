﻿namespace Cart.GraphQL.Scema.Mutations
{
    public class AddCartProductDataType
    {
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
