using Framewok.Core.Events;
using Framework.Kafka;
using Payment.Domain;
using Payment.Domain.MapProfile;
using Payment.Domain.Repositories;
using Payment.Domain.Services;
using Payment.GraphQL.Schema.Mutations;
using Payment.GraphQL.Schema.Queries;
using Query = Payment.GraphQL.Schema.Queries.Query;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDomainContext(builder.Configuration);
builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});
builder.Services.AddPayment();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

builder.Services
    .AddScoped<Query>()
    .AddScoped<PaymentQuery>()

    .AddScoped<Mutation>()
    .AddScoped<PaymentMutation>()

    .AddScoped<ICartProductRepository, CartProductRepository>()
    .AddScoped<ICartRepository, CartRepository>()
    .AddScoped<IProductRepository, ProductRepository>()

    .AddScoped<ICartService, CartService>()
    .AddGraphQLServer()

    .AddQueryType<Query>()
    .AddTypeExtension<PaymentQuery>()

    .AddMutationType<Mutation>()
    .AddTypeExtension<PaymentMutation>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();
