﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Entities
{
    public class CurrenciesEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string Code { get; set; } = default!;
        public string Symbol { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
