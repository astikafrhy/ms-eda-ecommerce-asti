﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Entities
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public string Sku { get; set; } 
        public string Name { get; set; } 
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public UserStatusEnum Status { get; set; }

    }
}
