﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain;
using Store.Domain.Repository;
using Store.Tests.MoqData;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Store.Tests.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDBContext _context;
        private readonly ITestOutputHelper _output;

        public TestCategoryRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<StoreDBContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new StoreDBContext(options);
            _context.Database.EnsureCreated();


            _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _context.Categories.AddRange(CategoryMoqData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            // Action
            var result = await repo.GetAll();
            var count = CategoryMoqData.GetCategories().Count();
            _output.WriteLine("Count : {0}", count);
            // Assert
            result.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetByIdAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _context.Categories.AddRange(CategoryMoqData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            // Action
            var result = await repo.GetById(new Guid("EB8BE168-E8F7-43DF-AED5-B9F7B742C3B9"));

            // Assert
            result.Name.Should().Be("Food");
        }

        [Fact]
        public async Task GetByIdFoodAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _context.Categories.AddRange(CategoryMoqData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            // Action
            var result = await repo.GetById(new Guid("3785797F-0E15-4584-BD88-41A234E2775F"));

            // Assert
            result.Name.Should().Be("Drink");
        }
        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
