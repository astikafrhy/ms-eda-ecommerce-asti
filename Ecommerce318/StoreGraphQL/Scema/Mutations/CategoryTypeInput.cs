﻿namespace StoreGraphQL.Scema.Mutations
{
    public class CategoryTypeInput
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
