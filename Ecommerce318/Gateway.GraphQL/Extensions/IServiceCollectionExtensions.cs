﻿using Microsoft.Extensions.DependencyInjection;

namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string LookUps = "LookUpService";
        public const string Stores = "StoreService";
        //public const string Carts = "CartService";
        //public const string Payments = "PaymentService";
        public const string Users = "UserService";
        
        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient(LookUps, c => c.BaseAddress = new Uri("https://localhost:7290/graphql"));
            services.AddHttpClient(Stores, c => c.BaseAddress = new Uri("https://localhost:7113/graphql"));
            services.AddHttpClient(Users, c => c.BaseAddress = new Uri("https://localhost:7279/graphql"));
            //services.AddHttpClient(Carts, c => c.BaseAddress = new Uri("https://localhost:7168/graphql"));
            //services.AddHttpClient(Payments, c => c.BaseAddress = new Uri("https://localhost:7100/graphql"));

            services
                .AddGraphQLServer()
                .AddRemoteSchema(LookUps)
                .AddRemoteSchema(Stores)
                //.AddRemoteSchema(Carts)
               // .AddRemoteSchema(Payments)
                .AddRemoteSchema(Users);
            return services;
        }

    }
}
