﻿using System.ComponentModel.DataAnnotations.Schema;


namespace Store.Domain.Entities.Configurations
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public string Sku { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; }

        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CategoryId")]
        public virtual CategoryEntity Category { get; set; }

        [ForeignKey("AttributeId")]
        public virtual AtributeEntity Attribute { get; set; }
    }
}
