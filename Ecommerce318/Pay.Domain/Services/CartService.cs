﻿using AutoMapper;
using Framewok.Core.Events;
using Framewok.Core.Events.Externals;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;
using Payment.Domain.Repositories;

namespace Payment.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> Paying(Guid cartId, decimal amount);
    }

    public class CartService : ICartService
    {
        private readonly ICartRepository _repository;
        private readonly ICartProductRepository _cartProductRepository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartService(
            ICartRepository repository,
            ICartProductRepository cartProductRepository,
            IProductRepository productRepository,
            IMapper mapper,
            IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _cartProductRepository = cartProductRepository;
            _productRepository = productRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.All());
        }

        public async Task<CartDto> Paying(Guid cartId, decimal amount)
        {
            CartDto dto = new CartDto();
            CartEntity entity = await _repository.GetById(cartId);

            if (entity != null)
            {
                if (entity.Total <= amount)
                {
                    entity.Pay = amount;
                    entity.Status = CartStatusEnum.Paid;

                    await _repository.Update(entity);
                    var list = await _cartProductRepository.GetByCartId(cartId);

                    //Checking stock
                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            if (product.Stock < item.Quantity)
                                return null;
                        }
                        else
                        {
                            return null;
                        }
                    }

                    List<CartProductItem> cartProducts = _mapper.Map<IEnumerable<CartProductItem>>(list).ToList();

                    //Update stock
                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            product.Stock = product.Stock - item.Quantity;

                            var selected = cartProducts.Where(o => o.ProductId == item.ProductId).FirstOrDefault();

                            if (selected != null)
                                selected.Stock = product.Stock;

                            await _productRepository.Update(product);
                        }
                        else
                        {
                            return null;
                        }
                    }

                    //var res = await _productRepository.SaveChangesAsync();
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<PaymentCreated>(
                            PaymentCreated.Create(
                                entity.Id,
                                cartProducts,
                                entity.Status
                            )
                        );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return _mapper.Map<CartDto>(entity);
                    }
                }
                return null;
            }
            return null;
        }
    }
}
