﻿using Cart.Domain.Entities;
using Framewok.Core.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Projections
{
    public record UserCreated(
      Guid Id,
      string UserName,
      string Password,
      string FirstName,
      string LastName,
      string Email,
      UserTypeEnum Type,
      UserStatusEnum Status,
      DateTime Modified
      );

    public record UserUpdated(
    Guid Id,
    string UserName,
    string Password,
    string FirstName,
    string LastName,
    string Email,
    DateTime Modified
    );

    public record UserStatusChanged(
        Guid Id,
        UserStatusEnum Status,
        DateTime Modified
        );

    public record UserTypeChanged(
       Guid Id,
       UserTypeEnum Type,
       DateTime Modified
       );

    public class UserProjection
    {
        public UserProjection()
        {

        }

        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, username, password, email, firstname, lastname, type, status, modified) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                UserEntity entity = new()
                {
                    Id = (Guid)id,
                    UserName = username,
                    Password = password,
                    Email = email,
                    FirstName = firstname,
                    LastName = lastname,
                    Type = type,
                    Status = status
                  
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }

            return true;
        }

        public static bool Handle(EventEnvelope<UserUpdated> eventEnvelope)
        {
            var (id, username, password, email, firstname, lastname, modified) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);

                entity.Id = (Guid)id;
                entity.UserName = username;
                entity.Password = password;
                entity.Email = email;
                entity.FirstName = firstname;
                entity.LastName = lastname;
                

                context.SaveChanges();
            }

            return true;
        }

        public static bool Handle(EventEnvelope<UserStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);

                entity.Id = (Guid)id;
                entity.Status = status;
                

                context.SaveChanges();
            }

            return true;
        }

        public static bool Handle(EventEnvelope<UserTypeChanged> eventEnvelope)
        {
            var (id, type, modified) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);

                entity.Id = (Guid)id;
                entity.Type = type;

                context.SaveChanges();
            }
            return true;
        }

    }

}
