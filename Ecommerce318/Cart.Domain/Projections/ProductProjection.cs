﻿using Cart.Domain.Entities;
using Framewok.Core.Events;

namespace Cart.Domain.Projections
{
    public record ProductCreated(
       Guid? Id,
       string Sku,
       string Name,
       decimal Price,
       decimal Volume,
       int Sold,
       int Stock,
       UserStatusEnum Status
       );

    public record ProductStatusChanged(
        Guid Id,
        UserStatusEnum Status
        );

    public record ProductUpdated(
        Guid Id,
        string SKU,
        string Name,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock
        );

    public class ProductProjection
    {
        public ProductProjection()
        {

        }
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, sku, name, price, volume, sold, stock, status) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    Sku = sku,
                    Name = name,
                    Price = price,
                    Volume = volume,
                    Sold = sold,
                    Stock = stock,
                    Status = status
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<ProductUpdated> eventEnvelope)
        {
            var (id, sku, name,price, volume, sold, stock) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                ProductEntity entity = context.Set<ProductEntity>().Find(id);
                entity.Id = id;
                entity.Sku = sku;
                entity.Name = name;
                entity.Price = price;
                entity.Volume = volume;
                entity.Sold = sold;
                entity.Stock = stock;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<ProductStatusChanged> eventEnvelope)
        {
            var (id, status) = eventEnvelope.Data;
            using (var context = new CartDBContext(CartDBContext.OnConfigure()))
            {
                ProductEntity entity = context.Set<ProductEntity>().Find(id);
                entity.Status = status;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }

}
