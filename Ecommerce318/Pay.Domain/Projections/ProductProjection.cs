﻿using Framewok.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record ProductCreated
    (
        Guid Id,
        decimal Price,
        int Stock,
        StoreStatusEnum Status
    );
    public class ProductProjection
    {
        public static bool Handle (EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, price, stock, status) = eventEnvelope.Data;
            using(var context = new PaymentDBContext(PaymentDBContext.OnConfigure()))
            {
                ProductEntity entity = new()
                {
                    Id = (Guid)id,
                    Price = price,
                    Stock = stock,
                    Status = status
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }
            return true;
            
        }
        
    }
}
