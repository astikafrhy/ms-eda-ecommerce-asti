﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Store.Domain.Migrations
{
    /// <inheritdoc />
    public partial class StoreProduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Atributes_AtributeId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_AtributeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AtributeId",
                table: "Products");

            migrationBuilder.CreateIndex(
                name: "IX_Products_AttributeId",
                table: "Products",
                column: "AttributeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Atributes_AttributeId",
                table: "Products",
                column: "AttributeId",
                principalTable: "Atributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Atributes_AttributeId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_AttributeId",
                table: "Products");

            migrationBuilder.AddColumn<Guid>(
                name: "AtributeId",
                table: "Products",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Products_AtributeId",
                table: "Products",
                column: "AtributeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Atributes_AtributeId",
                table: "Products",
                column: "AtributeId",
                principalTable: "Atributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
