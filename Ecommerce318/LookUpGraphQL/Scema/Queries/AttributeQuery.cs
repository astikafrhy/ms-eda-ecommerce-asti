﻿using HotChocolate.Authorization;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUpGraphQL.Scema.Queries
{
    [ExtendObjectType(typeof(Query))]
    public class AttributeQuery
    {
        private readonly IAtributeService _service;

        public AttributeQuery(IAtributeService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<AtributeDto>> GetAllAtributeAsync()
        {
            IEnumerable<AtributeDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AtributeDto> GetAttributeById(Guid id)
        {
            return await _service.GetAtributeById(id);
           
        }
    }
}
