﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<IEnumerable<CartEntity>> All();
        Task<CartEntity> GetById(Guid id);
        Task<CartEntity> Update(CartEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CartRepository : ICartRepository
    {
        protected readonly PaymentDBContext _context;
        public CartRepository(PaymentDBContext context)
        {
            _context = context;
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            return await _context.Set<CartEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartEntity> Update(CartEntity entity)
        {
            _context.Set<CartEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public async Task<IEnumerable<CartEntity>> All()
        {
            return await _context.Set<CartEntity>().Include("Customer").ToListAsync();
        }
    }
}
