﻿using HotChocolate.Authorization;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.GraphQL.Schema.Mutations;

namespace Store.GraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class CategoryMutation
    {
        private readonly ICategoryServices _service;

        public CategoryMutation(ICategoryServices service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDtos> AddCategoryAsync(CategoryTypeInput category)
        {
            CategoryDtos dto = new CategoryDtos();
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.AddCategory(dto);
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDtos> EditCategoryAsync(Guid id, CategoryTypeInput category)
        {
            CategoryDtos dto = new CategoryDtos();
            dto.Id = id;
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.UpdateCategory(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not found, 404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDtos> EditStatusCategoryAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateCategoryStatus(id, status);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not found, 404"));
            }
            return await _service.GetCategoryById(id);
        }

    }
}
