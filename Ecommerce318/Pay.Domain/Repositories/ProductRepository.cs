﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<ProductEntity> GetById(Guid id);
        Task<ProductEntity> Update(ProductEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class ProductRepository : IProductRepository
    {
        protected readonly PaymentDBContext _context;
        public ProductRepository(PaymentDBContext context)
        {
            _context = context;
        }
        public async Task<ProductEntity> GetById(Guid id)
        {
            return await _context.Set<ProductEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<ProductEntity> Update(ProductEntity entity)
        {
            _context.Set<ProductEntity>().Update(entity);
            return entity;
        }
    }
}
