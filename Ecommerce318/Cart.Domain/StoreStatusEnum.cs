﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Cart.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StoreStatusEnum
    {
        Active,
        Inactive,
        Removed
    }

}
