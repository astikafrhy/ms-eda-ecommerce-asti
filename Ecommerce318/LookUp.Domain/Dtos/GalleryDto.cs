﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Dtos
{
    public class GalleryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string FileLink { get; set; } = default!;
        public string Description { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default!;
    }
}
