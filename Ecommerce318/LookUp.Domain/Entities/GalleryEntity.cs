﻿namespace LookUp.Domain.Entities
{
    public class GalleryEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string FileLink { get; set; } = default!;
        public string Description { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
