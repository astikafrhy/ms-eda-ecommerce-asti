﻿using AutoMapper;
using LookUp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.Entities.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile(): base("Entity to Dto profile")
        {
            CreateMap<AtributeEntity, AtributeDto>();
            CreateMap<AtributeDto, AtributeEntity>();

            CreateMap<CurrenciesEntity, CurrenciesDto>();
            CreateMap<CurrenciesDto, CurrenciesEntity>();

            CreateMap<GalleryEntity, GalleryDto>();
            CreateMap<GalleryDto, GalleryEntity>();


        }
    }
}
