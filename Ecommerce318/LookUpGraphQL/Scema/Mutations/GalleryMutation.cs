﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using Path = System.IO.Path;

namespace LookUpGraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class GalleryMutation
    {
        private readonly IGalleryService _service;
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };


        public GalleryMutation(IGalleryService service)
        {
            _service = service;
        }

        public async Task<GalleryDto> AddGallery(GalleryTypeInput gallery)
        {
            try
            {
                GalleryDto dto = new GalleryDto();
                dto.Name = gallery.Name;
                dto.Description = gallery.Description;

                string fileExt = Path.GetExtension(gallery.File.Name);

                if(Array.IndexOf(acceptedExt, fileExt) != -1)
                {
                    var uniqueFileName = GetUniqueName(gallery.File.Name);
                    var uploads = Path.Combine("Resources", "Images");
                    var filePath = Path.Combine(uploads, uniqueFileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await gallery.File.CopyToAsync(fileStream);
                    }
                        dto.FileLink = uniqueFileName;

                }
                    return await _service.AddGallery(dto);

            }
            catch(Exception)
            {
                throw;
            }

        }

        public string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                + Path.GetExtension(fileName);
        }
    }
}
