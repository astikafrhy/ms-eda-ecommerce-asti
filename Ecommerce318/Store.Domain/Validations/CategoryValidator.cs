﻿using FluentValidation;
using Store.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Validations
{
    public class CategoryValidator: AbstractValidator<CategoryDtos>
    {
        public CategoryValidator()
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description minimun 5 chars");
        }

    }
}
