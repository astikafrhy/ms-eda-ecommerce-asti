﻿using FluentValidation;
using Store.Domain.Dtos;


namespace Store.Domain.Validations
{
    public class ProductValidator : AbstractValidator<ProductDtos>
    {
        public ProductValidator()
        {
            RuleFor(x => x.Sku).MinimumLength(3).WithMessage("Sku min 3 chars ").Matches("^[0-9]*$").WithMessage("Number only ");
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("description min 5 chars");
            //RuleFor(x => x.Price).ScalePrecision(18, 2).WithMessage("Price min 2");
           // RuleFor(x => x.Volume).ScalePrecision(18, 2).WithMessage("Volume min 2");

        }
    }
}
