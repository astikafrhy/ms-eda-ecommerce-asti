﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framewok.Core.BackgroundServices
{
    public class KafkaService : BackgroundService
    {
        private readonly ILogger<KafkaService> _logger;
        private readonly Func<CancellationToken, Task> _perform;

        public KafkaService(ILogger<KafkaService> logger, Func<CancellationToken, Task> perform)
        {
            _logger = logger;
            _perform = perform;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
          => Task.Run(async () => {
              await Task.Yield();
              _logger.LogInformation("Kafka service stopped");

              await _perform(stoppingToken);
              _logger.LogInformation("Kafka service stopped");
          }, stoppingToken);
    }
}
