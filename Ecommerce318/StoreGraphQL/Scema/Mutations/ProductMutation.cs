﻿using HotChocolate.Authorization;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace StoreGraphQL.Scema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class ProductMutation
    {
        private readonly IProductServices _service;

        public ProductMutation(IProductServices service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDtos> AddProductAsync(ProductTypeInput product)
        {
            ProductDtos dto = new ProductDtos();
            dto.Sku = product.Sku;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            var result = await _service.AddProduct(dto);
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDtos> EditProductAsync(Guid id, ProductTypeInput product)
        {
            ProductDtos dto = new ProductDtos();
            dto.Id = id;
            dto.Sku = product.Sku;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            var result = await _service.UpdateProduct(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDtos> UpdateProductStatusAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateProductStatus(id, status);
            if (result)
            {
                return await _service.GetProductById(id);
            }
            else
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));

            }

        }

    }
}

